// This file is autogenerated using scripts/generate-service-dashboards
// Please feel free to customize this file.
local platformLinks = import 'gitlab-dashboards/platform_links.libsonnet';
local serviceDashboard = import 'gitlab-dashboards/service_dashboard.libsonnet';

serviceDashboard.overview('ext-pvs').overviewTrailer() + {
  links+: [
    platformLinks.dynamicLinks('PVS Dashboards', 'type:pvs'),
    platformLinks.dynamicLinks('Runway Dashboards', 'type:runway'),
  ],
}
