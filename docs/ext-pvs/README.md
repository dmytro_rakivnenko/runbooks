<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Ext-pvs Service

* [Service Overview](https://dashboards.gitlab.net/d/ext-pvs-main/ext-pvs-overview)
* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22ext-pvs%22%2C%20tier%3D%22inf%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::ExtPVS"

## Logging

* [stackdriver](https://cloudlogging.app.goo.gl/2Nwee4eLCHa8Zcp87)

<!-- END_MARKER -->

<!-- ## Summary -->

External Pipeline Validation Service is in the process of being [migrated to Runway](https://gitlab.com/groups/gitlab-com/gl-infra/platform/runway/-/epics/1). During migration, refer to [documentation](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/pipeline-validation-service?ref_type=heads).

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
